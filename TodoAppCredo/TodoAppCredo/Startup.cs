﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TodoAppCredo.Startup))]
namespace TodoAppCredo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
