﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TodoAppCredo.Models
{
    public class TaskItem
    { public int id { get; set; }
        public string description { get; set; }
        public bool isDone { get; set; }
    }
}