﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TodoAppCredo.Models;

namespace TodoAppCredo.Controllers
{
    public class HomeController : Controller
    {
      
        TaskController t = new TaskController();
        public ActionResult Index()
        {
            ViewBag.Message = "Show To-Do list here!";
          
            return View(t.Get());
        }

        
    
        public ActionResult Add(string description)
        {
            t.Post(description);
            return RedirectToAction("Index");
           // return View();
        }
       
        public ActionResult Edit(int id)
        {
            TaskItem tEditing = t.Get(id);
            return View(tEditing);
        }
        public ActionResult Update(TaskItem item)
        {
            t.Put(item);
          //  t.Put(id, description);
            return RedirectToAction("Index");
            // return View();
        }
        public ActionResult Remove(int id)
        {
            ViewBag.Message = "RemoveTask here!";
            t.Delete(id);
            return RedirectToAction("Index");
           // return View();
        }
      
        
    }
}