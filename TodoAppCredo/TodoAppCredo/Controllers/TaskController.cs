﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TodoAppCredo.Models;

namespace TodoAppCredo.Controllers
{
    public class TaskController : ApiController
    {

        // GET: api/Task
        public IEnumerable<TaskItem> Get()
        {
            List<TaskItem> alltasks = new List<TaskItem>();

            using (SqlConnection myConnection = new SqlConnection())
            {
                myConnection.ConnectionString = @"Server=LAPTOP-PMBKFA86\SQLEXPRESS;Database=Todo;integrated security=True;";
                SqlCommand sqlCmd = new SqlCommand();
                sqlCmd.CommandType = CommandType.Text;
                sqlCmd.CommandText = "Select id, description, isDone from Tasklist";
                sqlCmd.Connection = myConnection;
                myConnection.Open();
                SqlDataReader reader = sqlCmd.ExecuteReader();
                TaskItem task = null;
                while (reader.Read())
                {
                    task = new TaskItem();
                    task.id = Convert.ToInt32(reader.GetValue(0));
                    task.description = reader.GetValue(1).ToString();
                    task.isDone = (bool)reader.GetValue(2);
                    alltasks.Add(task);
                }
            }
            return alltasks;
        }


        // GET: api/Task/5
        [HttpGet]
        public TaskItem Get(int id)
        {
            TaskItem task = null;
            using (SqlConnection myConnection = new SqlConnection())
            {
                myConnection.ConnectionString = @"Server=LAPTOP-PMBKFA86\SQLEXPRESS;Database=Todo;integrated security=True;";
                SqlCommand sqlCmd = new SqlCommand();
                sqlCmd.CommandType = CommandType.Text;
                sqlCmd.CommandText = "Select id, description, isDone from Tasklist where Id=@Id";
                sqlCmd.Parameters.AddWithValue("@Id", id);
                sqlCmd.Connection = myConnection;
                myConnection.Open();
                SqlDataReader reader = sqlCmd.ExecuteReader();
                
                while (reader.Read())
                {
                    task = new TaskItem();
                    task.id = Convert.ToInt32(reader.GetValue(0));
                    task.description = reader.GetValue(1).ToString();
                    task.isDone = (bool)reader.GetValue(2);

                }
            }

            return task;
        }




        // POST: api/Task
        [HttpPost]
        public void Post([FromBody]string value)
        {
            using (SqlConnection myConnection = new SqlConnection())
            {
                myConnection.ConnectionString = @"Server=LAPTOP-PMBKFA86\SQLEXPRESS;Database=Todo;integrated security=True;";
                SqlCommand sqlCmd = new SqlCommand();
                sqlCmd.CommandType = CommandType.Text;
                sqlCmd.CommandText = "INSERT INTO Tasklist (description,isDone) Values (@description,@isDone)";
                sqlCmd.Connection = myConnection;
                sqlCmd.Parameters.AddWithValue("@description", value);
                sqlCmd.Parameters.AddWithValue("@isDone", false);
                myConnection.Open();
                int rowInserted = sqlCmd.ExecuteNonQuery();
            }
        }

        // PUT: api/Task/5
        [HttpPut]
        public void Put(TaskItem item)
        {
            using (SqlConnection myConnection = new SqlConnection())
            {
                myConnection.ConnectionString = @"Server=LAPTOP-PMBKFA86\SQLEXPRESS;Database=Todo;integrated security=True;";
                SqlCommand sqlCmd = new SqlCommand();
                sqlCmd.CommandType = CommandType.Text;
                sqlCmd.CommandText = "update Tasklist set description=@description, isDone=@isDone where Id=@Id";
                sqlCmd.Connection = myConnection;
                sqlCmd.Parameters.AddWithValue("@description", item.description);
                sqlCmd.Parameters.AddWithValue("@isDone", item.isDone);
                sqlCmd.Parameters.AddWithValue("@Id", item.id);
                myConnection.Open();
                int rowInserted = sqlCmd.ExecuteNonQuery();
            }
                
        }


        // DELETE: api/Task/5
        [HttpDelete]
        public void Delete(int id)
        {
            using (SqlConnection myConnection = new SqlConnection())
            {
                myConnection.ConnectionString = @"Server=LAPTOP-PMBKFA86\SQLEXPRESS;Database=Todo;integrated security=True;";

                SqlCommand sqlCmd = new SqlCommand();
                sqlCmd.CommandType = CommandType.Text;
                sqlCmd.CommandText = "Delete from Tasklist where Id=@Id";
                sqlCmd.Connection = myConnection;
                sqlCmd.Parameters.AddWithValue("@Id", id);
                myConnection.Open();
                int rowInserted = sqlCmd.ExecuteNonQuery();
            }
        }
    }
}
