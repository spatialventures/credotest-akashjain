﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using TodoAppCredo.Models;

namespace TodoAppCredo.Controllers.Tests
{
    [TestClass()]
    public class HomeControllerTests
    {
        private Mock<TaskController> _mock;
        private TaskController _tController;
        [TestMethod()]
        public void IndexTest_UpdateTask()
        {//arrange
            _mock = new Mock<TaskController>();
            _tController = new TaskController();
            TaskItem item = new TaskItem()
            {
                id = 1,
                description = "first item",
                isDone = false
            };

            _mock.Setup(x => x.Put(item));
            _tController = new TaskController();
            //act
            _tController.Put(item);

        }

        [TestMethod()]
        public void IndexTest_DeleteTask()
        {//arrange
            _mock = new Mock<TaskController>();
            _tController = new TaskController();
            TaskItem item = new TaskItem()
            {
                id = 1,
                description = "first item",
                isDone = false
            };

            _mock.Setup(x => x.Delete(item.id));
            _tController = new TaskController();
            //act
            _tController.Delete(item.id);

        }


        [TestMethod()]
        public void AddTest_NewTask()
        {
            //arrange
            _mock = new Mock<TaskController>();
            _tController = new TaskController();
            var taskDescription = "new task to prep for interview";

            _mock.Setup(x => x.Post(taskDescription));
            _tController = new TaskController();
            //act
            _tController.Post(taskDescription);
        }

        [TestMethod()]
        public void EditTest()
        {
            //arrange
            _mock = new Mock<TaskController>();
            _tController = new TaskController();
            var taskId = 1;

            var item1 = _mock.Setup(x => x.Get(taskId));
            _tController = new TaskController();
            //act
            var item2 = _tController.Get(taskId);
            //assert
            Assert.AreEqual(item1, item2);
        }

        [TestMethod()]
        public void Index_GetAllTasksTest()
        {
            //arrange
            _mock = new Mock<TaskController>();
            _tController = new TaskController();
            var taskId = 1;

           var item1 = _mock.Setup(x => x.Get());
            _tController = new TaskController();
            //act
           var item2 = _tController.Get();
            //assert
            Assert.AreEqual(item1, item2);
        }




    }
}